// 1.Метод forEach дозволяє виконати задану функцію один раз для кожного елемента масиву
// 2.Щоб очистити масив в JavaScript, можна присвоїти йому порожній масив
// 3.Щоб перевірити, що змінна є масивом, можна використати метод Array.isArray()

function filterBy(arr, dataType) {
  return arr.filter(function(item) {
    return typeof item !== dataType;
  });
}
let myArray = ['hello', 'world', 23, '23', null];
let filteredArray = filterBy(myArray, 'string');

console.log(filteredArray);